#!/bin/sh

source send_cmd_pipe.sh
source script_parser.sh

ROOT_DEVICE=/dev/nandd
for parm in $(cat /proc/cmdline); do
    case $parm in
        root=*)
            ROOT_DEVICE=`echo $parm | awk -F\= '{print $2}'`
            ;;
    esac
done

BOOT_TYPE=-1
for parm in $(cat /proc/cmdline); do
    case $parm in
        boot_type=*)
            BOOT_TYPE=`echo $parm | awk -F\= '{print $2}'`
            ;;
    esac
done

case $ROOT_DEVICE in
    /dev/nand*)
        echo "nand boot"
        mount /dev/nanda /boot
        ;;
    /dev/mmc*)
        echo "mmc boot"
        case $BOOT_TYPE in
            2)
                echo "boot_type = 2"
                mount /dev/mmcblk0p2 /boot
                #SEND_CMD_PIPE_OK_EX $3
                #exit 1
                ;;
            *)
                echo "boot_type = $BOOT_TYPE"
                ;;
        esac
        ;;
    *)
        echo "default boot type"
        mount /dev/nanda /boot
        ;;
esac

PART_NUM=1

platform=`script_fetch "emmc" "platform"`
if [ "x$platform" = "xa64" ] ; then
	if [ ! -d "/sys/devices/soc.0/1c11000.sdmmc" ]; then
		SEND_CMD_PIPE_FAIL $3
   		 exit 1
	fi
else
	if [ "$BOOT_TYPE" -eq "1" ];then
		PART_NUM=8	
		if [ ! -d "/sys/devices/platform/sunxi-mmc.1" ]; then
			SEND_CMD_PIPE_FAIL $3
			echo "/sys/devices/platform/sunxi-mmc.1 is not exist"
   		 	exit 1
		fi
	else
		if [ ! -d "/sys/devices/platform/sunxi-mmc.2" ]; then
			SEND_CMD_PIPE_FAIL $3
			echo "/sys/devices/platform/sunxi-mmc.2 is not exist"
   			exit 1
		fi
	fi
fi
flashdev="/dev/mmcblk0p$PART_NUM"
echo "flashdev=$flashdev"

mkfs.vfat $flashdev
if [ $? -ne 0 ]; then
    SEND_CMD_PIPE_FAIL $3
    exit 1
fi

echo "create vfat file system for /dev/nanda done"

test_size=`script_fetch "emmc" "test_size"`
if [ -z "$test_size" -o $test_size -le 0 -o $test_size -gt $total_size ]; then
    test_size=64
fi

echo "test_size=$test_size"
echo "emmc test read and write"

emmcrw "$flashdev" "$test_size"
if [ $? -ne 0 ]; then
    SEND_CMD_PIPE_FAIL $3
    echo "emmc test fail!!"
else
    SEND_CMD_PIPE_OK_EX $3 
    echo "emmc test ok!!"
fi
